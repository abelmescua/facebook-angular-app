import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html'
})
export class DropdownComponent implements OnInit {
  items: any [] = ['Action', 'Another action', 'Something else'];

  constructor() { }

  ngOnInit() {
  }

}
