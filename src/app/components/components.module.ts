import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

// Components
import { PageComponent } from './page/page.component';
import { HeaderComponent } from './page/header/header.component';
import { MenuComponent } from './page/menu/menu.component';
import { SectionComponent } from './page/section/section.component';
import { DropdownComponent } from './page/header/dropdown/dropdown.component';
import { DatabaseServiceApi } from '../services/database.api.service';
import { CommentsComponent } from './page/section/comments/comments.component';

@NgModule({
  declarations: [
    PageComponent,
    HeaderComponent,
    MenuComponent,
    SectionComponent,
    DropdownComponent,
    CommentsComponent
  ],
  imports: [
      CommonModule,
      HttpClientModule
  ],
  // it's necessarry to export all the components in order the appModule can use them in AppComponent
  exports: [
    PageComponent,
    HeaderComponent,
    MenuComponent,
    SectionComponent
  ],
  providers: [DatabaseServiceApi]
})

export class ComponentsModule { }
